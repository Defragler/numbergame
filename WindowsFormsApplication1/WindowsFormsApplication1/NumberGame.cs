﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;

namespace NumberGame
{

    public class NumberDB
    {

        /// <summary>
        /// There actually isn't an existing database, I wasn't been able to realize this.
        /// So instead I've added a few statements that should make it easy to create the actual database
        /// </summary>
        public NumberDB()
        {

        }


        // CREATE DB IF NOT EXISTS numberDB
        // USE numberDB
        // if not exists(SELECT * FROM sysobjects WHERE name="StoredNumbers") CREATE TABLE dbo.StoredNumbers 
        // (
        //     ID int NOT NULL AUTO_INCREMENT,
        //     GUESS int NOT NULL,
        //     MIN int NOT NULL,
        //     MAX int NOT NULL
        //     PRIMARY KEY(ID)
        // )
        private SqlConnection myConnection = new SqlConnection("user id=username;" +
                                       "password=password;server=serverurl;" +
                                       "Trusted_Connection=yes;" +
                                       "database=numberDB; " +
                                       "connection timeout=30");

        public void insertNumber(int number)
        {
            try
            {
                if (myConnection.State == System.Data.ConnectionState.Closed)
                {
                    myConnection.Open();
                }

                SqlCommand command = new SqlCommand("INSERT INTO dbo.StoredNumbers(number, min, max) VALUES(@number, @min, @max)") { Connection = myConnection, CommandType = System.Data.CommandType.StoredProcedure };
                command.Prepare();

                command.Parameters.AddWithValue("@number", number);
                command.Parameters.AddWithValue("@min", NumberGameApp.Program.game.getMin());
                command.Parameters.AddWithValue("@max", NumberGameApp.Program.game.getMax());

                int rows = command.ExecuteNonQuery();
                Console.WriteLine(rows > 0 ? "succeeded" : "Failed");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            finally
            {
                myConnection.Close();
                Thread.CurrentThread.Abort();
            }
        }
    }

    public class Game
    {
        private readonly int min, max, correctNumber;
        private ObservableCollection<int> attempts;

        private static readonly Random random = new Random();

        public Game(int min, int max)
        {
            this.min = min;
            this.max = max;
            correctNumber = random.Next(getMin(), getMax() + 1);
            attempts = new ObservableCollection<int>();

            // Add listener
            attempts.CollectionChanged += (sender, e) =>
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    int guess = (int)e.NewItems[0];
                    this.guess(guess);
                    try
                    {
                        new Thread(delegate ()
                        {
                            NumberGameApp.Program.numberDB.insertNumber(guess);
                        }).Start();
                    }
                    catch (Exception exception) { Console.WriteLine(exception); }
                }
            };
        }

        public void addAttempt(int attempt)
        {
            this.attempts.Add(attempt);
        }

        public int getMin()
        {
            return this.min;
        }

        public int getMax()
        {
            return this.max;
        }

        public int getCorrectNumber()
        {
            return this.correctNumber;
        }

        public List<int> getAttempts()
        {
            return new List<int>(attempts);
        }

        public void guess(int number)
        {
            if (attempts.Count < 3)
            {
                if (number == correctNumber)
                {
                    MessageBox.Show("Congratulations, you have guessed the correct number.");
                    NumberGameApp.Program.startNewGame();
                    return;
                }
            }
            else
            {
                MessageBox.Show("You have exceeded maximum number of attempts. You have lost! the correct number was " + getCorrectNumber(), "You lose");
                NumberGameApp.Program.startNewGame();
                return;
            }
            NumberGameApp.Program.form.updateAttempts(3 - attempts.Count);
        }

    }
}
