﻿using NumberGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NumberGameApp
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static Game game = null;
        public static Form1 form = null;
        public static NumberDB numberDB = new NumberDB();
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(form = new Form1());

        }

        /// <summary>
        /// Starts a new game ( updates Frame labels, and creates a new game)
        /// </summary>
        public static void startNewGame()
        {
            game = new Game(5, 10);
            form.updateAttempts(3);
            form.updateRange(game.getMin(), game.getMax());
        }

    }
}
