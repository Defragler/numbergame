﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NumberGameApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Shown += new EventHandler(startGame);
        }

        private void startGame(object sender, EventArgs e)
        {
            try
            {
                new Thread(delegate ()
                {
                    Program.startNewGame();
                }).Start();
            }
            catch (Exception exception) { Console.WriteLine(exception); }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            int value = (int)numericUpDown1.Value;
            Program.game.addAttempt(value);
        }

        public void updateRange(int min, int max)
        {
            labelRange.Invoke(new Action(delegate { labelRange.Text = "The number is between " + min + " and " + max; }));
            numericUpDown1.Invoke(new Action(delegate { numericUpDown1.Minimum = min; numericUpDown1.Maximum = max; }));
        }

        public void updateAttempts(int amount)
        {
            labelAttempts.Invoke(new Action(delegate { labelAttempts.Text = "Attempts left: " + amount; }));
        }
    }
}
